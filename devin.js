'use strict'

// module readline-sync par défaut dans ./node_modules
const readlineSync = require('readline-sync')

function playermode() {
	/* Joueur humain doit trouver un nombre choisi par l’ordinateur */

	console.log("J'ai choisi un nombre compris entre 1 et 999.");
	// Définition proposition joueur
	let proposition;
	// Génération aléatoire un nombre entre 1 et 999
	const solution = Math.floor(Math.random() * (999 - 1)) + 1;
	// A commenter avant que le programme ne soit implémenté en production
	// console.log("*** La solution est " + solution);
	// Définition d'un compteur
	let count = 0;
	// On fait une boucle tant que la solution n'est pas trouvée on demande au joueur de proposer des solutions
	while (proposition !== solution) {
		count ++
		proposition = Number(readlineSync.question("Proposition " + count + " : "));
		if (proposition === solution) {
			console.log("Bravo ! Vous avez trouvé en " + count + " essais.", '\n');
		} else if ((proposition > solution) && (proposition < 1000)) {
			console.log("Trop grand !");
		} else if ((proposition < solution) && (proposition > 0)) {
			console.log("Trop petit !");
		} else {
			console.log("La réponse est un nombre entier entre 1 et 999...");
		}
	}
}

function computermode() {
	/* Joueur humain doit choisir un nombre et le faire deviner à l’ordinateur.	*/

	let numberchosen = readlineSync.question("Avez-vous choisi un nombre compris entre 1 et 999 (o/n) ? ")
	// le joueur conserve le nb choisi dans sa tête.
	if (numberchosen !== 'o') {
		computermode();
	} else {
		// Définition de l'indication du joueur (null à cause du premier passage)
		let indication;
		// Définition d'un compteur
		let count = 0;
		// Initialisation des bornes de la première proposition
		let maxpr = 999;
		let minpr = 1;
		let proposition;
		while (indication !== 't') {
			// l'ordi donne une proposition uniquement lors du premier passage (null) puis si l'indication est correcte
			if (indication === undefined || indication === 'p' || indication === 'g') {
				proposition = Math.floor(Math.random() * (maxpr - minpr)) + minpr
				count ++
				console.log("Proposition " + (count) + " : " + proposition)
			}
			indication = readlineSync.question("Trop (g)rand, trop (p)etit ou (t)rouvé ? ")
			if (indication === 't') {
				console.log("J'ai trouvé en " + count + " essais.", '\n');
			} else if (indication === 'g') {
				maxpr = proposition;
			} else if (indication === 'p') {
				minpr = proposition + 1;
			} else if (indication === '0') {
				let abandon = readlineSync.question("Voulez-vous vraiment abandonner (o/*) ? ");
				if (abandon === 'o') {
					indication = 't';
					console.log("Abandon de la partie !", '\n');
				} else {
					console.log("Proposition " + (count) + " : " + proposition)
				}
			} else {
				console.log("Je n'ai pas compris la réponse. Merci de répondre :\n\
\tg si ma proposition est trop grande\n\
\tp si ma proposition est trop petite\n\
\tt si j'ai trouvé le nombre");
			}
		}
	}
}

module.exports = {playermode, computermode}