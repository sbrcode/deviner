'use strict'
const devin = require('./devin')
const readlineSync = require('readline-sync')

/* Démarrage du jeu. Choix du mode */
let choice = undefined
while (choice !== 'o') {
	console.log("1- L'ordinateur choisit un nombre et vous le devinez\n\
2- Vous choissez un nombre et l'ordinateur le devine\n\
0- Quitter le programme");
	choice = readlineSync.question("Votre choix : ");
	switch (choice) {
		case '0':
			choice = readlineSync.question("Voulez-vous vraiment quitter (o/*) ? ");
			if (choice === 'o') {
				console.log("Au revoir...");
			}
			break;
		case '1':
			devin.playermode();
			break;
		case '2':
			devin.computermode();
			break;
	}
}