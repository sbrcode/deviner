#diviner's game

## prerequisites:
<npm install ...>

## Launch:
node devin_main.js

## Rules
The diviner's game is played with two players.<br>
The first player chooses a number between 1 and 999. The second must find it in a minimum of tries.<br>
With each proposal, the first player indicates whether the proposed number is greater or less than the number to be found.<br>
At the end, the number of tries required is displayed.
